# Makefile for cppvm
# Compiles the code for the VM
# Created by Andrew Davis
# Created on 5/7/2018
# Open source (MIT license)

# define the compiler
CXX=g++

# define the compiler flags
CXXFLAGS=-c -Wall -std=c++17

# define state-specific compiler flags
debug: CXXFLAGS += -g

# define linker flags
LDFLAGS=-lm

# retrieve the source code
MAIN=$(shell ls src/*.cpp)
ADT=$(shell ls src/adt/*.cpp)
EXCE=$(shell ls src/except/*.cpp)
UTIL=$(shell ls src/util/*.cpp)
MEM=$(shell ls src/mem/*.cpp)
OP=$(shell ls src/op/*.cpp)
HW=$(shell ls src/hw/*.cpp)

# list the source code
SOURCES=$(MAIN) $(ADT) $(EXCE) $(UTIL) $(MEM) $(OP) $(HW)

# compile the source code into object code
OBJECTS=$(SOURCES:.cpp=.o)

# define the name of the executable
EXECUTABLE=cppvm

# start of build targets

# target to compile the entire project without debug symbols
all: $(SOURCES) $(EXECUTABLE)

# target to build the executable without debug symbols
$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(OBJECTS) -o $@ $(LDFLAGS)
	mkdir obj
	mkdir bin
	mv -f $(OBJECTS) obj/
	mv -f $@ bin/

# target to build the executable with debug symbols
debug: $(OBJECTS)
	$(CXX) $(OBJECTS) -o $(EXECUTABLE) $(LDFLAGS)
	mkdir obj
	mkdir bin
	mv -f $(OBJECTS) obj/
	mv -f $(EXECUTABLE) bin/

# target to compile source code into object code
.cpp.o:
	$(CXX) $(CXXFLAGS) $< -o $@

# target to install the compiled executable
# REQUIRES ROOT
install:
	cp ./bin/$(EXECUTABLE) /usr/bin/

# target to clean the workspace
clean:
	rm -rf ./bin
	rm -rf ./obj

# end of Makefile
