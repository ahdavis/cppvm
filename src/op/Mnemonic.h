/*
 * Mnemonic.h
 * Enumerates mnemonics of opcodes
 * Created by Andrew Davis
 * Created on 5/14/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//include
#include <string>

//enum definition
enum class Mnemonic {
	NOP = 0x00, //no operation
	ADD, //add two registers
	SUB, //subtract one register from another
	MPY, //multiply one register by another
	DIV, //divide one register by another
	MOD, //calculate one register modulus another
	INR, //read 16-bit value into a register
	OTR, //output the value in a register
	//TODO: Insert other opcodes here
	EOP //end of opcodes
};

//returns a Mnemonic's string equivalent
std::string mnemonicToString(Mnemonic m); 

//end of enum
