/*
 * Mnemonic.cpp
 * Implements a function that converts a mnemonic to a string
 * Created by Andrew Davis
 * Created on 5/14/2018
 * Open source (MIT license)
 */

//include header
#include "Mnemonic.h"

//mnemonicToString function - converts a mnemonic to a string
std::string mnemonicToString(Mnemonic m) {
	//switch on the mnemonic
	switch(m) {
		case Mnemonic::NOP: 
			{
				return "NOP";
			}
		case Mnemonic::ADD:
			{
				return "ADD";
			}
		case Mnemonic::SUB:
			{
				return "SUB";
			}
		case Mnemonic::MPY:
			{
				return "MPY";
			}
		case Mnemonic::DIV:
			{
				return "DIV";
			}
		case Mnemonic::MOD:
			{
				return "MOD";
			}
		case Mnemonic::INR:
			{
				return "INR";
			}
		case Mnemonic::OTR:
			{
				return "OTR";
			}
		default:
			{
				return "Unknown opcode";
			}
	}
}

//end of implementation
