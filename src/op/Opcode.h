/*
 * Opcode.h
 * Declares a class that represents an opcode
 * Created by Andrew Davis
 * Created on 5/14/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//includes
#include <string>
#include <cstdint>
#include "Mnemonic.h"
#include "../except/OpcodeException.h"

//class declaration
class Opcode final {
	//public fields and methods
	public:
		//default constructor
		Opcode();

		//main constructor
		explicit Opcode(Mnemonic newMnemonic);

		//destructor
		~Opcode();

		//copy constructor
		Opcode(const Opcode& oc);

		//assignment operator
		Opcode& operator=(const Opcode& src);

		//getter method
		
		//returns the mnemonic of the opcode
		Mnemonic getMnemonic() const;

		//static methods
		
		//returns an Opcode instance associated with a byte
		static Opcode withCode(std::uint8_t code);

		//returns an Opcode instance associated with a string name
		static Opcode withName(const std::string& name);

	//private fields and methods
	private:
		//field
		Mnemonic mnemonic; //the mnemonic of the opcode
		
};

//end of header
