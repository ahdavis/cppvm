/*
 * Opcode.cpp
 * Implements a class that represents an opcode
 * Created by Andrew Davis
 * Created on 5/14/2018
 * Open source (MIT license)
 */

//include header
#include "Opcode.h"

//default constructor
Opcode::Opcode()
	: Opcode(Mnemonic::NOP) //call the other constructor
{
	//no code needed
}

//main constructor
Opcode::Opcode(Mnemonic newMnemonic)
	: mnemonic(newMnemonic) //init the field
{
	//no code needed
}

//destructor
Opcode::~Opcode() {
	//no code needed
}

//copy constructor
Opcode::Opcode(const Opcode& oc)
	: mnemonic(oc.mnemonic) //copy the field
{
	//no code needed
}

//assignment operator
Opcode& Opcode::operator=(const Opcode& src) {
	this->mnemonic = src.mnemonic; //assign the mnemonic
	return *this; //and return the instance
}

//getMnemonic method - returns the mnemonic of the opcode
Mnemonic Opcode::getMnemonic() const {
	return this->mnemonic; //return the mnemonic field
}

//static withCode method - returns an opcode referenced by a one-byte code
Opcode Opcode::withCode(std::uint8_t code) {
	//declare a mnemonic to be matched with the code
	Mnemonic op = Mnemonic::EOP;

	//loop and match the code to a mnemonic
	for(int c = static_cast<int>(Mnemonic::NOP); 
			c < static_cast<int>(Mnemonic::EOP); c++) {
		//get the code
		std::uint8_t tCode = static_cast<std::uint8_t>(c);

		//check for a match
		if(tCode == code) { //if the codes match
			op = static_cast<Mnemonic>(c); //get the mnemonic
		
			break; //and exit the loop
		}
	}

	//make sure that the code matched
	if(op == Mnemonic::EOP) { //if no mnemonic was found
		throw OpcodeException(code); //then throw an exception
	}

	//return an opcode from the mnemonic
	return Opcode(op);
}

//static withName method - returns an Opcode referenced by a string name
Opcode Opcode::withName(const std::string& name) {
	//declare a mnemonic to be matched with the name
	Mnemonic op = Mnemonic::EOP;

	//loop and get the mnemonic
	for(int c = static_cast<int>(Mnemonic::NOP);
			c < static_cast<int>(Mnemonic::EOP); c++) {
		//get the name of the current opcode
		std::string tName = mnemonicToString(
						static_cast<Mnemonic>(c));

		//match the names
		if(tName == name) { //if the names match
			//then get the mnemonic
			op = static_cast<Mnemonic>(c); 

			//and exit the loop
			break;
		}
	}

	//make sure that a mnemonic was found
	if(op == Mnemonic::EOP) { //if no match was found
		throw OpcodeException(name); //then throw an exception
	}

	//return an opcode from the mnemonic
	return Opcode(op);
}

//end of implementation
