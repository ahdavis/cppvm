/*
 * bytesToWord.h
 * Declares a function that converts two bytes to a 16-bit word
 * Created by Andrew Davis
 * Created on 5/9/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//include
#include <cstdint>

//function declaration
std::uint16_t bytesToWord(std::uint8_t msb, std::uint8_t lsb);

//end of header
