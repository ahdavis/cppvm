/*
 * wordToBytes.cpp
 * Implements a function that converts a 16-bit word to two bytes
 * Created by Andrew Davis
 * Created on 5/9/2018
 * Open source (MIT license)
 */

//include header
#include "wordToBytes.h"

//wordToBytes function - converts a 16-bit word to two bytes
std::pair<std::uint8_t, std::uint8_t> wordToBytes(std::uint16_t word) {
	//get the bytes from the word
	std::uint8_t msb = static_cast<std::uint8_t>(word >> 8); 
	std::uint8_t lsb = static_cast<std::uint8_t>(word); 

	//and combine the bytes into a pair
	return std::make_pair(msb, lsb);
}

//end of implementation
