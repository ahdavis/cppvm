/*
 * wordToBytes.h
 * Declares a function that converts a 16-bit word to two bytes
 * Created by Andrew Davis
 * Created on 5/9/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//includes
#include <cstdint>
#include <utility>

//function declaration
std::pair<std::uint8_t, std::uint8_t> wordToBytes(std::uint16_t word);

//end of header
