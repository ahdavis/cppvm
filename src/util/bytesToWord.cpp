/*
 * bytesToWord.cpp
 * Implements a function that converts two bytes to a 16-bit word
 * Created by Andrew Davis
 * Created on 5/9/2018
 * Open source (MIT license)
 */

//include header
#include "bytesToWord.h"

//bytesToWord function - converts two bytes to a 16-bit word
std::uint16_t bytesToWord(std::uint8_t msb, std::uint8_t lsb) {
	std::uint16_t ret = 0x0000; //declare the return value

	//convert the bytes to a word
	ret = msb;
	ret = ret << 8;
	ret |= lsb;

	//and return the word
	return ret;
}

//end of implementation
