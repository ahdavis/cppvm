/*
 * Register.cpp
 * Implements a class that represents a hardware register
 * Created by Andrew Davis
 * Created on 5/14/2018
 * Open source (MIT license)
 */

//include header
#include "Register.h"

//first constructor
Register::Register(RegName newName)
	: Register(newName, 0x0000) //call the other constructor
{
	//no code needed
}

//second constructor
Register::Register(RegName newName, std::uint16_t newValue)
	: name(newName), value(newValue) //init the fields
{
	//no code needed
}

//destructor
Register::~Register() {
	//no code needed
}

//copy constructor
Register::Register(const Register& r)
	: name(r.name), value(r.value) //copy the fields
{
	//no code needed
}

//assignment operator
Register& Register::operator=(const Register& src) {
	this->name = src.name; //assign the name field
	this->value = src.value; //assign the value field
	return *this; //and return the instance
}

//getName method - returns the name of the Register
RegName Register::getName() const {
	return this->name; //return the name field
}

//getValue method - returns the value of the Register
std::uint16_t Register::getValue() const {
	return this->value; //return the value field
}

//getIndex method - returns the index of the Register
int Register::getIndex() const {
	return static_cast<int>(this->name); //return the name as an int
}

//setValue method - sets the value of the Register
void Register::setValue(std::uint16_t newValue) {
	this->value = newValue; //set the value field
}

//inc method - increments the value of the Register
void Register::inc() {
	this->value++; //increment the value field
}

//dec method - decrements the value of the Register
void Register::dec() {
	this->value--; //decrement the value field
}

//add method - adds another register's value to the Register
void Register::add(const Register& other) {
	this->value += other.value;
}

//sub method - subtracts another register's value from the Register
void Register::sub(const Register& other) {
	this->value -= other.value;
}

//mpy method - multiplies another register's value by the Register
void Register::mpy(const Register& other) {
	this->value *= other.value;
}

//div method - divides the Register by another register's value
void Register::div(const Register& other) {
	this->value /= other.value;
}

//mod method - calculates the Register modulus another register's value
void Register::mod(const Register& other) {
	this->value %= other.value;
}

//end of implementation
