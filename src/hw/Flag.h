/*
 * Flag.h
 * Declares a class that represents a hardware flag
 * Created by Andrew Davis
 * Created on 5/15/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//includes
#include "FlagType.h"
#include "FlagState.h"

//class declaration
class Flag final {
	//public fields and methods
	public:
		//first constructor - initializes the state as cleared
		explicit Flag(FlagType newType);

		//second constructor - specifies a starting state
		Flag(FlagType newType, FlagState newState);

		//destructor
		~Flag();

		//copy constructor
		Flag(const Flag& f);

		//assignment operator
		Flag& operator=(const Flag& src);

		//getter method
		
		//returns the type of the flag
		FlagType getType() const;

		//other methods
		
		//returns whether the flag is set
		bool isSet() const;

		//returns whether the flag is cleared
		bool isCleared() const;

		//sets the flag
		void set();

		//clears the flag
		void clear();

	//private fields and methods
	private:
		//fields
		FlagType type; //the type of the flag
		FlagState state; //the state of the flag
};

//end of header
