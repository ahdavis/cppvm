/*
 * Flag.cpp
 * Implements a class that represents a hardware flag
 * Created by Andrew Davis
 * Created on 5/15/2018
 * Open source (MIT license)
 */

//include header
#include "Flag.h"

//first constructor
Flag::Flag(FlagType newType)
	: Flag(newType, FlagState::CLR) //call the other constructor
{
	//no code needed
}

//second constructor
Flag::Flag(FlagType newType, FlagState newState)
	: type(newType), state(newState) //init the fields
{
	//no code needed
}

//destructor
Flag::~Flag() {
	//no code needed
}

//copy constructor
Flag::Flag(const Flag& f)
	: type(f.type), state(f.state) //copy the fields
{
	//no code needed
}

//assignment operator
Flag& Flag::operator=(const Flag& src) {
	this->type = src.type; //assign the type field
	this->state = src.state; //assign the state field
	return *this; //and return the instance
}

//getType method - returns the type of the flag
FlagType Flag::getType() const {
	return this->type; //return the type field
}

//isSet method - returns whether the flag is set
bool Flag::isSet() const {
	//return whether the flag is set
	return this->state == FlagState::SET;
}

//isCleared method - returns whether the flag is cleared
bool Flag::isCleared() const {
	//return whether the flag is cleared
	return this->state == FlagState::CLR;
}

//set method - sets the flag
void Flag::set() {
	this->state = FlagState::SET; //set the flag
}

//clear method - clears the flag
void Flag::clear() {
	this->state = FlagState::CLR; //clear the flag
}

//end of implementation
