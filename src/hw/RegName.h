/*
 * RegName.h
 * Enumerates VM registers
 * Created by Andrew Davis
 * Created on 5/14/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//no includes

//enum definition
enum class RegName {
	PC, //program counter
	A, //accumulator
	B,
	C,
	D,
	E,
	F
};

//end of enum
