/*
 * FlagType.h
 * Enumerates types of hardware flags
 * Created by Andrew Davis
 * Created on 5/15/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//no includes

//enum definition
enum class FlagType {
	ZERO, //zero flag
	EQU, //equals flag
	LT, //less than flag
	GT, //greater than flag
	ODD, //odd flag
	EVN //even flag
};

//end of enum
