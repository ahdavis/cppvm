/*
 * Register.h
 * Declares a class that represents a hardware register
 * Created by Andrew Davis
 * Created on 5/14/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//includes
#include "RegName.h"
#include <cstdint>

//class declaration
class Register final {
	//public fields and methods
	public:
		//first constructor - initializes the register's value
		//to 0x0000
		explicit Register(RegName newName);

		//second constructor
		Register(RegName newName, std::uint16_t newValue);

		//destructor
		~Register();

		//copy constructor
		Register(const Register& r);

		//assignment operator
		Register& operator=(const Register& src);

		//getter methods
		
		//returns the name of the register
		RegName getName() const;

		//returns the value of the register
		std::uint16_t getValue() const;

		//returns the index of the register
		int getIndex() const;

		//setter method
		
		//sets the value of the register
		void setValue(std::uint16_t newValue);

		//other methods
		
		//increments the register's value
		void inc();

		//decrements the register's value
		void dec();

		//adds another register's value to the register
		void add(const Register& other);

		//subtracts another register's value from the register
		void sub(const Register& other);

		//multiplies another register's value by the register
		void mpy(const Register& other);

		//divides the register by another register's value
		void div(const Register& other);

		//calculates the register modulus another register's value
		void mod(const Register& other);

	//private fields and methods
	private:
		//fields
		RegName name; //the name of the register
		std::uint16_t value; //the value of the register
};

//end of header
