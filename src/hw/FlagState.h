/*
 * FlagState.h
 * Enumerates hardware flag states
 * Created by Andrew Davis
 * Created on 5/15/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//no includes

//enum definition
enum class FlagState {
	SET, //flag is set
	CLR //flag is cleared
};

//end of enum
