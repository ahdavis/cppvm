/*
 * VRAM.cpp
 * Implements a class that represents virtual memory
 * Created by Andrew Davis
 * Created on 5/8/2018
 * Open source (MIT license)
 */

//include header
#include "VRAM.h"

//init the constants
const unsigned int VRAM::DEFAULT_SIZE = 0x10000; //init the default size
const std::uint16_t VRAM::PROG_START = 0x0000; //init the program start

//default constructor
VRAM::VRAM()
	: VRAM(VRAM::DEFAULT_SIZE) //call the other constructor
{
	//no code needed
}

//main constructor
VRAM::VRAM(unsigned int newSize)
	: data(nullptr), readPos(0x0000), size(newSize) //init the fields
{
	//init the data pointer
	this->data = new std::uint8_t[newSize];

	//and zero it out
	this->zero();
	this->readPos = 0x0000;
}

//destructor
VRAM::~VRAM() {
	this->free(); //deallocate VRAM
}

//copy constructor
VRAM::VRAM(const VRAM& v)
	: data(nullptr), readPos(v.readPos), size(v.size) //copy the fields
{
	//copy the data pointer
	this->data = new std::uint8_t[v.size]; //init the data pointer
	for(unsigned int i = 0; i < v.size; i++) { //loop and copy the data
		this->data[i] = v.data[i];
	}
}

//assignment operator
VRAM& VRAM::operator=(const VRAM& src) {
	this->readPos = src.readPos; //assign the read position
	this->size = src.size; //assign the size

	//assign the data pointer
	this->free();
	this->data = new std::uint8_t[this->size];
	for(unsigned int i = 0; i < this->size; i++) {
		this->data[i] = src.data[i];
	}

	//and return the instance
	return *this;
}

//getReadPos method - returns the current read position
std::uint16_t VRAM::getReadPos() const {
	return this->readPos; //return the read position field
}

//getSize method - returns the number of allocated bytes
unsigned int VRAM::getSize() const {
	return this->size; //return the size field
}

//setReadPos method - sets the read position
void VRAM::setReadPos(std::uint16_t newPos) {
	this->readPos = newPos; //update the read position
}

//peek method - returns the byte at a given address
std::uint8_t VRAM::peek(std::uint16_t addr) {
	//verify the address
	if(addr >= this->size) { //if the address is out of range
		//then throw an exception
		throw AccessException(addr);
	}

	//set the read position
	this->readPos = addr;

	//and return the byte at the address
	return this->data[addr];
}

//poke method - sets the byte at a given address
void VRAM::poke(std::uint8_t byte, std::uint16_t addr) {
	//verify the address
	if(addr >= this->size) { //if the address is out of range
		//then throw an exception
		throw AccessException(addr);
	}

	//set the read position
	this->readPos = addr;

	//and set the byte at the address
	this->data[addr] = byte;
}

//retrieveBytes method - retrieves a sequence of bytes ending in a
//terminator byte from memory
std::vector<std::uint8_t> VRAM::retrieveBytes(std::uint16_t addr,
						std::uint8_t term) {
	//declare a return value
	std::vector<std::uint8_t> ret;

	//declare an address index
	std::uint16_t idx = addr;

	//loop and retrieve bytes
	while(true) {
		std::uint8_t byte = this->peek(idx); //get a byte

		ret.push_back(byte); //add it to the return vector

		//determine whether to exit the loop
		if(byte == term) { //if the terminator byte was found
			break; //then exit the loop
		}

		//increment the address index
		idx++;
	}

	//and return the bytes
	return ret;
}

//storeBytes method - stores a vector of bytes in memory
void VRAM::storeBytes(const std::vector<std::uint8_t>& bytes,
			std::uint16_t addr) {
	//get an address index
	std::uint16_t idx = addr;
	
	//loop and store the bytes
	for(const std::uint8_t& b : bytes) {
		this->poke(b, idx++);
	}
	
}

//retrieveInt method - retrieves a 16-bit integer from memory
std::uint16_t VRAM::retrieveInt(std::uint16_t addr) {
	//get the bytes that make up the integer
	std::uint8_t msb = this->peek(addr);
	std::uint8_t lsb = this->peek(addr + 1);

	//and return the integer composed of the bytes
	return bytesToWord(msb, lsb);
}

//storeInt method - stores a 16-bit integer in memory
void VRAM::storeInt(std::uint16_t val, std::uint16_t addr) {
	//get the bytes that make up the integer
	std::pair<std::uint8_t, std::uint8_t> bPair = wordToBytes(val);

	//convert the pair to a vector
	std::vector<std::uint8_t> bytes = {bPair.first, bPair.second};

	//and store the vector
	this->storeBytes(bytes, addr);
}

//retrieveString method - retrieves a string from memory
std::string VRAM::retrieveString(std::uint16_t addr) {
	//retrieve the string bytes from memory
	std::vector<std::uint8_t> bytes = this->retrieveBytes(addr, '\0');

	//loop and assemble the string
	std::stringstream ss;
	for(const std::uint8_t& b : bytes) {
		ss << b;
	}

	//assign a string from the bytes
	std::string ret = ss.str();

	//and return it
	return ret;
}

//storeString method - stores a string in memory
void VRAM::storeString(const std::string& str, std::uint16_t addr) {
	//declare a vector to hold the string's bytes
	std::vector<std::uint8_t> bytes;

	//declare a string index
	int idx = 0;

	//loop and load the string into the vector
	while(true) {
		//put the character into the vector
		char b = str[idx++]; 
		bytes.push_back(static_cast<std::uint8_t>(b));

		//determine whether to exit the loop
		if(b == '\0') {
			break;
		}
	}

	//and store the vector
	this->storeBytes(bytes, addr);
}

//zero method - zeroes the bytes in memory
void VRAM::zero() {
	//loop and zero the bytes
	for(unsigned int i = 0; i < this->size; i++) {
		this->poke('\0', i);
	}
}

//private free method - deallocates VRAM
void VRAM::free() {
	delete[] this->data; //deallocate the data pointer
	this->data = nullptr; //and zero it out
}

//end of implementation
