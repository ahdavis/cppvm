/*
 * VRAM.h
 * Declares a class that represents virtual memory
 * Created by Andrew Davis
 * Created on 5/8/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//includes
#include <cstdint>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include "../except/AccessException.h"
#include "../util/bytesToWord.h"
#include "../util/wordToBytes.h"

//class declaration
class VRAM final {
	//public fields and methods
	public:
		//constant
		
		//address of the start of program memory
		static const std::uint16_t PROG_START;

		//default constructor
		VRAM();

		//main constructor
		explicit VRAM(unsigned int newSize);

		//destructor
		~VRAM();

		//copy constructor
		VRAM(const VRAM& v);

		//assignment operator
		VRAM& operator=(const VRAM& src);

		//getter methods
		
		//returns the current read position
		std::uint16_t getReadPos() const;

		//returns the number of bytes allocated for VRAM
		unsigned int getSize() const;

		//setter method
		
		//sets the read position
		void setReadPos(std::uint16_t newPos);

		//other methods
		
		//returns the byte at a given address
		std::uint8_t peek(std::uint16_t addr);

		//sets the byte at a given address
		void poke(std::uint8_t byte, std::uint16_t addr);

		//retrieves a sequence of bytes ending in a terminator
		//byte from memory
		std::vector<std::uint8_t> retrieveBytes(std::uint16_t addr,
							std::uint8_t term);

		//stores bytes in memory
		void storeBytes(const std::vector<std::uint8_t>& bytes,
						std::uint16_t addr);

		//retrieves a 16-bit integer from memory
		std::uint16_t retrieveInt(std::uint16_t addr);

		//stores a 16-bit integer in memory
		void storeInt(std::uint16_t val, std::uint16_t addr);

		//retrieves a string from memory
		std::string retrieveString(std::uint16_t addr);

		//stores a string in memory
		void storeString(const std::string& str, 
					std::uint16_t addr);

		//zeroes out VRAM
		void zero();

	//private fields and methods
	private:
		//method
		void free(); //deallocates the memory

		//constant
		
		//the default number of bytes allocated
		static const unsigned int DEFAULT_SIZE;

		//fields
		std::uint8_t* data; //the actual bytes
		std::uint16_t readPos; //the address of the current byte
		unsigned int size; //the number of allocated bytes
		
};

//end of header
