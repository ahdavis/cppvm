/*
 * OpcodeException.cpp
 * Implements an exception that is thrown when 
 * an invalid opcode is referenced
 * Created by Andrew Davis
 * Created on 5/14/2018
 * Open source (MIT license)
 */

//include header
#include "OpcodeException.h"

//first constructor
OpcodeException::OpcodeException(std::uint8_t badCode)
	: errMsg() //init the field
{
	//assemble the error message
	std::stringstream ss;
	ss << "Invalid opcode 0x";
	ss << std::hex << badCode;
	this->errMsg = ss.str();
}

//second constructor
OpcodeException::OpcodeException(const std::string& badName)
	: errMsg() //init the field
{
	//assemble the error message
	std::stringstream ss;
	ss << "Invalid opcode ";
	ss << badName;
	this->errMsg = ss.str();
}

//destructor
OpcodeException::~OpcodeException() {
	//no code needed
}

//copy constructor
OpcodeException::OpcodeException(const OpcodeException& oe)
	: errMsg(oe.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
OpcodeException& OpcodeException::operator=(const OpcodeException& src) {
	this->errMsg = src.errMsg; //assign the field
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* OpcodeException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
