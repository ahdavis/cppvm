/*
 * StackException.cpp
 * Implements an exception that is thrown 
 * when an empty stack is popped from
 * Created by Andrew Davis
 * Created on 5/7/2018
 * Open source (MIT license)
 */

//include the header
#include "StackException.h"

//destructor
StackException::~StackException() {
	//no code needed
}

//overridden what method - called when the exception is thrown
const char* StackException::what() const throw() {
	//return an error message
	return "Stack::pop() called on an empty stack";
}

//end of implementation
