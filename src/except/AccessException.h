/*
 * AccessException.h
 * Declares an exception that is thrown when 
 * out-of-range memory is accessed
 * Created by Andrew Davis
 * Created on 5/8/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//includes
#include <cstdint>
#include <exception>
#include <iostream>
#include <sstream>
#include <string>

//class declaration
class AccessException final : public std::exception
{
	//public fields and methods
	public:
		//constructor
		explicit AccessException(std::uint16_t badAddress);

		//destructor
		~AccessException();

		//copy constructor
		AccessException(const AccessException& ae);

		//assignment operator
		AccessException& operator=(const AccessException& src);

		//called when the exception is thrown
		const char* what() const throw() override;

	//private fields and methods
	private:
		//field
		std::string errMsg; //the error message
};

//end of header
