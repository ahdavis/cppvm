/*
 * StackException.h
 * Declares an exception that is thrown when an empty stack is popped from
 * Created by Andrew Davis
 * Created on 5/7/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//include
#include <exception>

//class declaration
class StackException final : public std::exception
{
	//public fields and methods
	public:
		//constructor is defaulted
		
		//destructor
		~StackException();

		//copy constructor is defaulted
		
		//assignment operator is defaulted

		//called when the exception is thrown
		const char* what() const throw() override;

	//no private fields or methods
};

//end of header
