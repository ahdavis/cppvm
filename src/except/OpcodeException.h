/*
 * OpcodeException.h
 * Declares an exception that is thrown when 
 * an invalid opcode is referenced
 * Created by Andrew Davis
 * Created on 5/14/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//includes
#include <string>
#include <exception>
#include <cstdint>
#include <sstream>
#include <iostream>

//class declaration
class OpcodeException final : public std::exception
{
	//public fields and methods
	public:
		//first constructor - uses a one-byte opcode value
		explicit OpcodeException(std::uint8_t badCode);

		//second constructor - uses a string opcode name
		explicit OpcodeException(const std::string& badName);

		//destructor
		~OpcodeException();

		//copy constructor
		OpcodeException(const OpcodeException& oe);

		//assignment operator
		OpcodeException& operator=(const OpcodeException& src);

		//called when the exception is thrown
		const char* what() const throw() override;

	//private fields and methods
	private:
		//field
		std::string errMsg; //the error message
};

//end of header
