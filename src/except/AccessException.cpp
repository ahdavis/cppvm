/*
 * AccessException.cpp
 * Implements an exception that is thrown when 
 * out-of-range memory is accessed
 * Created by Andrew Davis
 * Created on 5/8/2018
 * Open source (MIT license)
 */

//include header
#include "AccessException.h"

//constructor
AccessException::AccessException(std::uint16_t badAddress)
	: errMsg() //init the field
{
	//assemble the error message
	std::stringstream ss;
	ss << "Invalid memory address found! Address: 0x";
	ss << std::hex << badAddress;
	this->errMsg = ss.str();
}

//destructor
AccessException::~AccessException() {
	//no code needed
}

//copy constructor
AccessException::AccessException(const AccessException& ae)
	: errMsg(ae.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
AccessException& AccessException::operator=(const AccessException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* AccessException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
