/*
 * INode.h
 * Declares a class that represents a linked list node
 * Created by Andrew Davis
 * Created on 5/7/2018
 * Open source (MIT license)
 */

//include guard
#pragma once 

//no includes

//class declaration
template<typename T>
class Node final {
	//public fields and methods
	public:
		//first constructor
		explicit Node(T newData);

		//second constructor
		Node(T newData, const Node<T>* newNext);

		//destructor
		~Node();

		//copy constructor
		Node(const Node<T>& n);

		//assignment operator
		Node<T>& operator=(const Node<T>& src);

		//getter methods
		
		//returns the Node's data
		T getData() const; 

		//returns the next Node in the list
		Node<T>* getNext();

	//private fields and methods
	private:
		//method
		void free(); //deallocates the Node

		//fields
		T data; //the Node's data
		Node<T>* next; //the next node in the list
		
};

//end of header
