/*
 * Stack.h
 * Include file for the Stack ADT
 * Created by Andrew Davis
 * Created on 5/7/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//includes
#include "IStack.h"
#include "StackImpl.cpp"

//end of header
