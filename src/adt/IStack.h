/*
 * IStack.h
 * Declares a class that represents a stack
 * Created by Andrew Davis
 * Created on 5/7/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//includes
#include "Node.h"
#include "../except/StackException.h"

//class declaration
template<typename T>
class Stack final {
	//public fields and methods
	public:
		//constructor
		Stack();

		//destructor
		~Stack();

		//copy constructor
		Stack(const Stack<T>& s);

		//assignment operator
		Stack<T>& operator=(const Stack<T>& src);

		//methods
		
		//puts a value on top of the Stack
		void push(T value);

		//removes the value on top of the Stack and returns it
		T pop();

		//returns the value on top of the Stack
		T peek() const;

		//returns the number of items in the Stack
		int getSize() const;

		//returns whether the Stack is empty
		bool isEmpty() const;

		//clears the Stack
		void clear();

	//private fields and methods
	private:
		//method
		void free(); //deallocates the Stack

		//fields
		Node<T>* top; //the Node that contains the top of the Stack
		int size; //the number of items in the Stack 
};

//end of header
