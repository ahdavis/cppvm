/*
 * NodeImpl.cpp
 * Implements a class that represents a linked list node
 * Created by Andrew Davis
 * Created on 5/7/2018
 * Open source (MIT license)
 */

//include header
#include "INode.h"

//first constructor
template<typename T>
Node<T>::Node(T newData)
	: Node(newData, nullptr) //call the other constructor
{
	//no code needed
}

//second constructor
template<typename T>
Node<T>::Node(T newData, const Node<T>* newNext)
	: data(newData), next(nullptr) //init the fields
{
	//init the next node pointer
	if(newNext != nullptr) { //if the new next node is not null
		//then copy it
		this->next = new Node<T>(*newNext);
	}
}

//destructor
template<typename T>
Node<T>::~Node<T>() {
	this->free(); //deallocate the node
}

//copy constructor
template<typename T>
Node<T>::Node(const Node<T>& n)
	: data(n.data), next(nullptr) //copy the fields
{
	//copy the next node pointer
	if(n.next != nullptr) { //if the other next node is not null
		this->next = new Node<T>(*n.next); //then copy it
	}
}

//assignment operator
template<typename T>
Node<T>& Node<T>::operator=(const Node<T>& src) {
	this->data = src.data; //assign the data field

	//assign the next node
	this->free(); //deallocate the current node
	if(src.next != nullptr) { //if the other next node is not null
		this->next = new Node<T>(*src.next); //then copy it
	}

	//and return the object
	return *this;
}

//getData method - returns the data of the Node
template<typename T>
T Node<T>::getData() const {
	return this->data; //return the data field
}

//getNext method - returns the next Node in the list
template<typename T>
Node<T>* Node<T>::getNext() {
	return this->next; //return the next node in the list
}

//private free method - deallocates the node
template<typename T>
void Node<T>::free() {
	delete this->next; //deallocate the next node
	this->next = nullptr; //and zero it out
}

//end of implementation
