/*
 * Node.h
 * Include file for the Stack ADT's Node class
 * Created by Andrew Davis
 * Created on 5/7/2018
 * Open source (MIT license)
 */

//include guard
#pragma once

//includes
#include "INode.h"
#include "NodeImpl.cpp"

//end of header
