/*
 * StackImpl.cpp
 * Implements a class that represents a stack
 * Created by Andrew Davis
 * Created on 5/7/2018
 * Open source (MIT license)
 */

//include header
#include "IStack.h"

//constructor
template<typename T>
Stack<T>::Stack()
	: top(nullptr), size(0) //init the fields
{
	//no code needed
}

//destructor
template<typename T>
Stack<T>::~Stack<T>() {
	this->free(); //deallocate the Stack
}

//copy constructor
template<typename T>
Stack<T>::Stack(const Stack<T>& s)
	: top(nullptr), size(s.size) //copy the fields
{
	//copy the top element of the Stack
	if(s.top != nullptr) { //if the other top element is not null
		this->top = new Node<T>(*s.top); //then copy it
	}
}

//assignment operator
template<typename T>
Stack<T>& Stack<T>::operator=(const Stack<T>& src) {
	this->free(); //deallocate the stack
	
	//assign the top element
	if(src.top != nullptr) { //if the other top element is not null
		this->top = new Node<T>(*src.top); //then copy it
	}

	this->size = src.size; //assign the size field
	return *this; //and return the instance
}

//push method - puts a value on top of the Stack
template<typename T>
void Stack<T>::push(T value) {
	//put the value on top of the stack
	if(this->top == nullptr) { //if the stack is empty
		this->top = new Node<T>(value); //then add the value
	} else { //if the stack is not empty
		//then create a temporary node
		Node<T>* temp = new Node<T>(value, this->top);

		//deallocate the stack
		this->free();

		//and assign the top element to the temporary node 
		this->top = temp;
	}

	//and increment the size counter
	this->size++;
}

//pop method - removes the value on top of the Stack and returns it
template<typename T>
T Stack<T>::pop() {
	T ret = this->peek(); //get the value on top of the stack

	//remove the top value
	if(this->size == 1) { //if there is only one element in the stack
		this->free(); //then clear the stack
	} else { //if there is more than one element in the stack
		//then get the next node
		Node<T>* temp = 
			new Node<T>(this->top->getNext()->getData(),
					this->top->getNext());

		//deallocate the current node
		this->free();

		//and assign the next node to the current node
		this->top = temp;
	}

	//decrement the size field
	this->size--;

	//and return the top object
	return ret;
}

//peek method - returns the value on top of the stack
template<typename T>
T Stack<T>::peek() const {
	//determine whether the stack is empty
	if(this->top == nullptr) { //if the stack is empty
		//then throw an exception
		throw StackException();
	} else { //if the stack is not empty
		//then return the top Node's value
		return this->top->getData();
	}
}

//getSize method - returns the size of the stack
template<typename T>
int Stack<T>::getSize() const {
	return this->size; //return the size field
}

//isEmpty method - returns whether the stack is empty
template<typename T>
bool Stack<T>::isEmpty() const {
	return this->top == nullptr; //return whether the top is null
}

//clear method - clears the stack
template<typename T>
void Stack<T>::clear() {
	//loop and clear the stack
	while(!this->isEmpty()) {
		this->pop();
	}
}

//private free method - deallocates the stack
template<typename T>
void Stack<T>::free() {
	delete this->top; //deallocate the top node
	this->top = nullptr; //and zero it out
}

//end of implementation
