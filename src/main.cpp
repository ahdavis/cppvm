/*
 * main.cpp
 * Main code file for cppvm
 * Created by Andrew Davis
 * Created on 5/7/2018
 * Open source (MIT license)
 */

//includes
#include <iostream>
#include <cstdlib>
#include <string>
#include <cstdint>
#include "mem/VRAM.h"

//main function - main entry point for the program
int main(int argc, char* argv[]) {
	//declare a VRAM instance
	VRAM memory;
	
	//declare constants
	const std::uint16_t intAddr = 0xDDDD;
	const std::uint16_t strAddr = 0xEEEE;

	//display the first prompt
	std::cout << "Enter an integer: ";
	
	//read in an integer
	std::uint16_t val = 0;
	std::cin >> val;

	//consume the trailing newline
	std::cin.get();

	//store the integer in memory
	memory.storeInt(val, intAddr);

	//display the second prompt
	std::cout << "Enter a string: ";

	//read in a string
	std::string str;
	std::getline(std::cin, str);

	//store the string in memory
	memory.storeString(str, strAddr);

	//retrieve the integer from memory
	val = memory.retrieveInt(intAddr);

	//retrieve the string from memory
	str = memory.retrieveString(strAddr);

	//print out the retrieved values
	std::cout << "The integer you entered is: " << val << std::endl;
	std::cout << "The string you entered is: " << str << std::endl;

	//and return with no errors
	return EXIT_SUCCESS;
}

//end of program
